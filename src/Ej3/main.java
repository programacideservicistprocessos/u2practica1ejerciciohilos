package Ej3;

public class main {
    public static void main(String[] args) {
        Thread[] caballos = new Thread[11];
        for(int i = 1; i <= 10; i++){
            if(i == 1){
                caballos[i] = new Thread(new ThreadHorse(i, 25));
                caballos[i].start();
            }else if(i == 10){
                caballos[i] = new Thread(new ThreadHorse(i, 15));
                caballos[i].start();
            }else{
                caballos[i] = new Thread(new ThreadHorse(i));
                caballos[i].start();
            }
        }
        for(int i = 1; i <= 10; i++){
            try {
                caballos[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
