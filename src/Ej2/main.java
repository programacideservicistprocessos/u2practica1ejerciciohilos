package Ej2;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Dame un número par: ");
        int id = sc.nextInt();

        if(id%2==0) {
            Thread pokemons = new Thread(new Pokemons(id));
            pokemons.start();
        }else{
            System.out.println("Error: Numero impar");
        }
    }
}

