package Ej2;

public class Pokemons implements Runnable{

        private int id;
        public Pokemons(int id) {
            this.id = id;
        }

        @Override
        public void run() {
            for(int i = 1; i <= id; i++) {
                System.out.println("Soy el entrenador número " + i);
                System.out.println("Soy el pokémon número " + i + "\n");
            }
        }

}
