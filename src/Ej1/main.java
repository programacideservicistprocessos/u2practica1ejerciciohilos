package Ej1;

public class main {
    public static void main(String[] args) {
        Thread[] caballos = new Thread[11];
        for(int i = 1; i <= 10; i++){
            caballos[i] = new Thread(new ThreadHorse(i));
            caballos[i].start();
        }
        for(int i = 1; i <= 10; i++){
            try {
                caballos[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
